#ifndef DOGE2D_ALL_H
#define DOGE2D_ALL_H

/*
 * DOGE2D:
 * A collection of functions and tools to help me make my game code simpler.
 * Contains numerous pieces of code inspired by the TwinklebearDev Tutorial,
 * but obviously converted into C.
 * https://www.willusher.io/pages/sdl2/
 */
#include "doge_common.h"
#include "doge_vec2.h"
#include "doge_framelimit.h"
/* TYPES */

/* Error codes. */
enum doge_ExitCode {
	DOGE_FAIL = -1,
	DOGE_SUCCESS = 0,
};


#define DOGE_MAX_PATH 261 /* Somewhat arbitrary, based on WIN32 path limit. */

/* Init flags used by DOGE. */
#ifndef DOGE_SDL_INIT_FLAGS
#define DOGE_SDL_INIT_FLAGS (SDL_INIT_EVERYTHING)
#endif

#ifndef DOGE_IMG_INIT_FLAGS
#define DOGE_IMG_INIT_FLAGS (IMG_INIT_PNG|IMG_INIT_JPG)
#endif

/* Path separator. */
#ifdef _WIN32
#   define DOGE_PATH_SEP '\\'
#else
#   define DOGE_PATH_SEP '/'
#endif

/* FUNCTIONS 
 *
 * Batch processing functions such as doge_load_textures are preferred,
 * since using them will be faster than repeated calls to the individual functions,
 * and an 'array of one' can be mocked up to use them even for a single item.
 *
 * I am more likely to actively maintain these batch functions as a result.
 *
 * However, I have only created these when it is common for there to be many things
 * that want to be processed in the same way, such as many textures being loaded from names.
 */

/* BASICS */

int doge_init(void); /* Establishes some initial globals for the doge2d translation unit, including base path. Returns DOGE_FAIL on failure. */
int doge_exit_with_sdl(int exit_code); /* Quits SDL, prints an error message and returns exit_code. Will attempt to destroy the window and renderer passed to doge_configure_* */


/* These functions give DOGE access to the window and renderer, allowing it to use or free them later. */

/* Configures the window for DOGE to use if not specified. Returns DOGE_FAIL on failure. */
int doge_configure_window(SDL_Window* win);
/* Configures the renderer for DOGE to use if not specified. Returns DOGE_FAIL on failure. */
int doge_configure_renderer(SDL_Renderer* ren);


/* Path utilities */
char* doge_get_parent_path(const char* file_path); /* Returns the parent directory of the given file path. Ignores trailing path separator. The result is heap-allocated. Returns NULL on failure. */
char* doge_get_res_path(const char* rel_path); /* Returns the absolute path of a resource with its path relative to the parent of the base_path, separated with '/' regardless of system. */

/* TODO: Delete this!
 * Inefficient and probably unsafe use of string functions in here! Avoid use if possible. Functionality can be efficiently manually inlined in other functions.
 * This function is designed to create lots of duplicate copies of doge_base_path in memory, with different relative paths prepended, making it terrible.
 * DO NOT USE!
 * */
char** doge_get_res_paths(const char** rel_paths, const int count); /* Returns the absolute paths of a set of resources with their paths relative to the parent of the base_path, separated with '/' regardless of system. */


/* Textures */
SDL_Texture* doge_load_texture(const char* file, SDL_Renderer* ren); /* Loads an image file (with its relative path given) into a texture, using IMG_Load */
SDL_Texture** doge_load_textures(const char** files, const int file_count, SDL_Renderer* ren); /* Loads a series of image files (with their relative paths given) into textures, using IMG_Load */
int doge_free_textures(SDL_Texture** textures, const int texture_count); /* Destroys every texture pointed to in the array. Does NOT free the array itself. Created before DOGE_DESTROY_LIST; should be deprecated. */
void doge_render_texture(SDL_Renderer* ren, SDL_Texture* tex, int x, int y); /* A restrictive function that blits a texture at its unscaled resolution with the renderer. */

/* Fonts and Text */
SDL_Texture* doge_render_text(TTF_Font* font, const char* text, SDL_Color col, SDL_Renderer* ren);

/* Tilemap
 * A powerful feature of DOGE is/will be tilemap rendering.
 * The basic principles are as follows:
 * Most of the time, the contents of a tilemap remain constant,
 * so the tile layout should be rendered into a single large texture
 * which can then be rendered with a single call to SDL_RenderCopy.
 * If the contents of the map need to be changed if, for example,
 * a block is destroyed,
 * the tilemap information can be edited and the texture simply re-rendered.
 *
 * Therefore, a texture renderer must be created.
 *
 * Aspirationally, an algorithm to automatically create a collision map for the tilemap,
 * with a small number of rectangles, could be employed to futher aid efficiency.
 */

typedef struct doge_Tileset {
    SDL_Texture* tiles;
    Uint32 tile_size;
    Sint32 width;
    Sint32 height;
    float scale;
} doge_Tileset;

typedef struct doge_Tilemap {
    struct doge_Tileset* tileset;
    Uint32 width;
    Uint32 height;
    struct doge_v2ui* layout;
} doge_Tilemap;

struct doge_Tileset* doge_create_tileset(const char* rel_path, const Sint32 tile_size, const float scale, SDL_Renderer* ren); /* Does what it says. */
void doge_destroy_tileset(struct doge_Tileset* tileset);

struct doge_Tilemap* doge_create_tilemap(struct doge_Tileset* tileset, struct doge_v2ui* layout, Uint32 width, Uint32 height);
void doge_destroy_tilemap(struct doge_Tilemap* tilemap);

void doge_render_tilemap(struct doge_Tilemap* tilemap, Sint32 x, Sint32 y, SDL_Renderer* ren);

/* Type-Generic Cleanup Helpers */
#define DOGE_DESTROY(X) _Generic((X),		\
		SDL_Texture*: SDL_DestroyTexture,		\
		SDL_Window*: SDL_DestroyWindow,		\
		SDL_Renderer*: SDL_DestroyRenderer,		\
		TTF_Font*: TTF_CloseFont, \
		doge_Tileset*: doge_destroy_tileset, \
		default: SDL_free)(X)

/* This is probably a bad idea, but it's better than lots of code duplication, in my view. */
#define DOGE_DESTROY_LIST(X, LEN) \
for (uint32_t __DOGE_I = 0; __DOGE_I < LEN; ++__DOGE_I) { \
    DOGE_DESTROY(X[__DOGE_I]); \
}

#endif
// vim: ft=c
