#ifndef DOGE_FRAMES_H
#define DOGE_FRAMES_H

#include "doge_common.h"
#include <SDL2/SDL_timer.h>

#define DOGE_DEFAULT_FRAME_TIME_LIMIT (1000/60) /* A sensible default delay if you're lazy. */

void doge_set_frame_time(Sint32 frametime); /* Set the frametime limit/target. */
void doge_limit_frames(); /* Limit the framerate to a fixed value.*/
void doge_limit_frames_proc(void (*wait_proc)()); /* Limit the framerate to a fixed value. wait_proc will be executed repeatedly until the required time has elapsed. */

#endif
