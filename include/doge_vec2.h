/*
 * DOGE Vec2:
 * We obviously need vectors.
 */

#include "doge_common.h"

typedef struct doge_v2f {
    float x;
    float y;
} doge_v2f;

typedef struct doge_v2i {
    Sint32 x;
    Sint32 y;
} doge_v2i;

typedef struct doge_v2ui {
    Uint32 x;
    Uint32 y;
} doge_v2ui;

struct doge_v2ui doge_v2ui_mul(doge_v2ui vec, float multiplier);
struct doge_v2ui doge_v2ui_add(doge_v2ui a, doge_v2ui b);
