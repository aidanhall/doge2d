DOGE2D

A framework for making 2D games with SDL 2.
I'm not calling this a game engine because for the foreseeable future it exists
merely as a set of useful functions that must be called manually by the user.

I'm mostly creating this because I think it will probably make it cleaner
if I separate my 'game code' and 'engine code' for the games I create.

Also, I think it's cool.
