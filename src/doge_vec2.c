#ifndef DOGE_VEC2_H
#define DOGE_VEC2_H

#include <doge_common.h>
#include <doge_vec2.h>

struct doge_v2ui doge_v2ui_mul(doge_v2ui vec, float multiplier) {
    return (struct doge_v2ui){.x = vec.x*multiplier, .y = vec.y*multiplier};
}

struct doge_v2ui doge_v2ui_add(doge_v2ui a, doge_v2ui b) {
    return (struct doge_v2ui) {.x =a.x + b.x, .y =a.y+b.y};
}
#endif
