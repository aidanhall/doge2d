#include <doge_framelimit.h>

static Uint32 delay = DOGE_DEFAULT_FRAME_TIME_LIMIT;
static Sint32 last_time = 0;

void doge_set_frame_time(Sint32 frametime) {
    if (frametime > 0 ) {
	delay = frametime;
    }
}

void doge_limit_frames_proc(void (*wait_proc)()) {
    while (!SDL_TICKS_PASSED(SDL_GetTicks(), last_time + delay)) {
	wait_proc();
    }
}

void doge_limit_frames() {
    while (!SDL_TICKS_PASSED(SDL_GetTicks(), last_time + delay));
}
