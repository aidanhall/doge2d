#include <doge2d.h>

/* Globals. Probably a bad idea? */
static const char* doge_base_path;
static const char* doge_parent_base_path;

static SDL_Window* doge_window = NULL;
static SDL_Renderer* doge_ren = NULL;

int doge_init(void) {

    /* SDL */
    if ( SDL_Init(DOGE_SDL_INIT_FLAGS) != 0) {
	SDL_LogError(SDL_LOG_CATEGORY_ERROR, "SDL_Init failed: %s\n",
		SDL_GetError());
	return DOGE_FAIL;
    }

    /* IMG */
    if ((IMG_Init(DOGE_IMG_INIT_FLAGS) & DOGE_IMG_INIT_FLAGS) != DOGE_IMG_INIT_FLAGS) {
	SDL_LogError(SDL_LOG_CATEGORY_ERROR, "IMG_Init failed: %s\n",
		IMG_GetError());
	return DOGE_FAIL;
    }

    /* TTF */
    if ( TTF_Init() != 0 ) {
	SDL_LogError(SDL_LOG_CATEGORY_ERROR, "TTF_Init failed: %s\n",
		TTF_GetError());
	return DOGE_FAIL;
    }

    /* Paths */
    doge_base_path = SDL_GetBasePath();
    if (doge_base_path == NULL) {
	return DOGE_FAIL;
    }
    doge_parent_base_path = doge_get_parent_path(doge_base_path);
    if (doge_parent_base_path == NULL) {
	return DOGE_FAIL;
    }

    return DOGE_SUCCESS;
}


int doge_configure_window(SDL_Window* win) {
    enum doge_ExitCode ret_val = DOGE_SUCCESS;

    if (doge_window == NULL) {
	if ( win == NULL ) {
	    SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Window uninitialised: %s\n", SDL_GetError());
	    ret_val = DOGE_FAIL;
	} else {
	    doge_window = win;
	}
    } else {
	SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Doge window already set.\n");
	ret_val = DOGE_FAIL;
    }

    return ret_val;
}

int doge_configure_renderer(SDL_Renderer* ren) {
    enum doge_ExitCode ret_val = DOGE_SUCCESS;
    if (doge_ren == NULL) {
	if ( ren == NULL ) {
	    SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Renderer uninitialised: %s\n", SDL_GetError());
	    ret_val = DOGE_FAIL;
	} else {
	    doge_ren = ren;
	}
    } else {
	SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Doge renderer already set.\n");
	ret_val = DOGE_FAIL;
    }

    return ret_val;
}


int doge_exit_with_sdl(int exit_code) {
    SDL_Log("DOGE says goodbye!");
    if (exit_code != 0) {
	SDL_Log(" %s\n", SDL_GetError());
    }

    /* const is only ever a suggestion anyway */
    /* TODO: Don't use (char*) to suppress warnings!!! This is a terrible idea!!! */
    SDL_Log("Freeing path strings.\n");
    SDL_free((char*)doge_base_path);
    SDL_free((char*)doge_parent_base_path);

    SDL_Log("Freeing window.\n");
    if (doge_window != NULL) {
	SDL_DestroyWindow(doge_window);
    }
    SDL_Log("Freeing renderer.\n");
    if (doge_ren != NULL) {
	SDL_DestroyRenderer(doge_ren);
    }

    SDL_Log("Quitting TTF.\n");
    if (TTF_WasInit()) {
	TTF_Quit();
    }

    SDL_Log("Quitting IMG.\n");
    IMG_Quit();
    SDL_Log("Quitting SDL.\n");
    SDL_Quit();

    /* Return value intended to be used as exit code for program. */
    return exit_code;
}

char* doge_get_parent_path(const char* file_path) {
    const unsigned int length = strnlen(file_path, DOGE_MAX_PATH);
    char* parent_path = NULL;
    if (length == DOGE_MAX_PATH) {
	/* Too long or no null termination. */
    } else {
	/* Start from the second-last non-null character to ignore trailing path separators. */
	for (int index = length-2; index >= 0; --index) {
	    if (file_path[index] == DOGE_PATH_SEP) {
		/* The parent path will include a trailing separator to make other functions simpler. */

		parent_path = strndup(file_path, index+1);
		index = -1;
	    }
	}
    }

    return parent_path;
}

char* doge_get_res_path(const char* rel_path) {
    /* The idea is that rel_path is the only string the user has to create, and can always be /-separated. */
    if (rel_path[0] == DOGE_PATH_SEP || rel_path[0] == '~') {
	return strndup(rel_path, DOGE_MAX_PATH);
    }

    char* res_path;

    { // Get res path.
	res_path = SDL_malloc(
		(strnlen(doge_parent_base_path, DOGE_MAX_PATH) + strnlen(rel_path, DOGE_MAX_PATH) + 1 ) * sizeof(char));

	/* Dangerous string stuff, but I should be able to trust
	 * doge_parent_base_path since my function produced it. */
	strcpy(res_path, doge_parent_base_path);

	/* strcat(res_path, DOGE_PATH_SEP); */
	strcat(res_path, rel_path);
    }

#ifdef _WIN32
    { // Windows path separators.
	char* last_forward_slash;
	while ((SDL_strrchr(res_path, '/')) != NULL) {
	    res_path[last_forward_slash] = DOGE_PATH_SEP;
	}
    }
#endif

    return res_path;
}

char** doge_get_res_paths(const char** rel_paths, const int count) {
    /* The idea is that rel_path is the only string the user has to create, and can always be /-separated. */
    char** res_paths = SDL_malloc(count * sizeof (char**));

    for (int i = 0; i < count; ++i ){ // Get res path.
	res_paths[i] = SDL_malloc(
		(strnlen(doge_parent_base_path, DOGE_MAX_PATH) + strnlen(rel_paths[i], DOGE_MAX_PATH) + 1 ) * sizeof(char));

	/* Dangerous string stuff, but I should be able to trust
	 * doge_parent_base_path since my function produced it. */
	strcpy(res_paths[i], doge_parent_base_path);

	/* strcat(res_path, DOGE_PATH_SEP); */
	strcat(res_paths[i], rel_paths[i]);

#ifdef _WIN32
	{ // Windows path separators.
	    char* last_forward_slash;
	    while ((SDL_strrchr(res_path, '/')) != NULL) {
		res_path[last_forward_slash] = DOGE_PATH_SEP;
	    }
	}
#endif
    }

    return res_paths;
}

SDL_Texture* doge_load_texture(const char* file, SDL_Renderer* ren) {
    SDL_Texture* tex = NULL;
    char* file_path = doge_get_res_path(file);
    SDL_LogDebug(SDL_LOG_CATEGORY_TEST, "File path: %s\n", file_path);

    {
	// Load image into surface.
	SDL_Surface *bmp = IMG_Load(file_path);
	if (bmp == NULL) {
	    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "Couldn't create surface.");
	} else {
	    tex = SDL_CreateTextureFromSurface(ren, bmp);
	    SDL_free(bmp);
	}
    }
    SDL_free(file_path);
    return tex;
}

SDL_Texture** doge_load_textures(const char** files, const int count, SDL_Renderer* ren) {
    SDL_Texture** textures = SDL_calloc(count , sizeof(SDL_Texture*));

    /* char** file_paths = doge_get_res_paths(files, count); */

    char* current_file_path = SDL_malloc(DOGE_MAX_PATH * sizeof (char));
    /* TODO: Guarantee that this is safe. */
    char* base_back = stpcpy(current_file_path, doge_parent_base_path);


    for (int i = 0; i < count; ++i) {
	/* TODO: See above. */
	strcpy(base_back, files[i]);
	SDL_Surface *surface = IMG_Load(current_file_path); 

	if (surface == NULL) {
	    SDL_LogError(SDL_LOG_CATEGORY_RENDER, "Couldn't create surface.");
	} else {
	    textures[i] = SDL_CreateTextureFromSurface(ren, surface);
	    SDL_free(surface);
	}
    }

    SDL_free(current_file_path);

    return textures;
}

int doge_free_textures(SDL_Texture** textures, const int texture_count) {
    for (int i = 0; i < texture_count; ++i) {
	SDL_DestroyTexture(textures[i]);
    }
    return DOGE_SUCCESS;
}

void doge_render_texture(SDL_Renderer* ren, SDL_Texture* tex, int x, int y) {
    //Setup the destination rectangle to be at the position we want
    SDL_Rect dst;
    dst.x = x;
    dst.y = y;
    //Query the texture to get its width and height to use
    SDL_QueryTexture(tex, NULL, NULL, &dst.w, &dst.h);
    SDL_RenderCopy(ren, tex, NULL, &dst);
}

SDL_Texture* doge_render_text(TTF_Font* font, const char* text, SDL_Color col, SDL_Renderer* ren) {
    SDL_Surface* text_surface = TTF_RenderText_Solid(font, text, col);
    if (text_surface == NULL) {
	return NULL;
    }

    SDL_Texture* text_texture = SDL_CreateTextureFromSurface(ren, text_surface);

    SDL_free(text_surface);
    return text_texture;
}

struct doge_Tileset* doge_create_tileset(const char* rel_path, const Sint32 tile_size, const float scale, SDL_Renderer* ren) {
    struct doge_Tileset* tileset = SDL_malloc(sizeof(struct doge_Tileset));

    tileset->tiles = doge_load_texture(rel_path, ren);
    if (tileset->tiles == NULL) {
	SDL_free(tileset);
	return NULL;
    }

    tileset->tile_size = tile_size;
    if ( SDL_QueryTexture(tileset->tiles, NULL, NULL, &tileset->width, &tileset->height) ) {
	SDL_free(tileset->tiles);
	SDL_free(tileset);
	return NULL;
    }

    tileset->scale = scale;

    return tileset;

}

void doge_destroy_tileset(struct doge_Tileset* tileset) {
    SDL_DestroyTexture(tileset->tiles);
    SDL_free(tileset);
}


struct doge_Tilemap* doge_create_tilemap(struct doge_Tileset* tileset, struct doge_v2ui* layout, Uint32 width, Uint32 height) {
    struct doge_Tilemap* tilemap = SDL_malloc(sizeof(struct doge_Tilemap));

    tilemap->tileset = tileset;
    tilemap->layout = layout;
    tilemap->width = width;
    tilemap->height = height;

    return tilemap;
}

void doge_destroy_tilemap(struct doge_Tilemap* tilemap) {
    SDL_free(tilemap->layout);
    SDL_free(tilemap);
}

void doge_render_tilemap(struct doge_Tilemap* tilemap, Sint32 x, Sint32 y, SDL_Renderer* ren) {
    SDL_Rect tile_region = {.w = tilemap->tileset->tile_size, .h = tilemap->tileset->tile_size};
    /* Multiplying by scale provides a quick and dirty way of scaling the entire tilemap. */
    SDL_Rect dest_region = {.w = tilemap->tileset->tile_size * tilemap->tileset->scale, .h = tilemap->tileset->tile_size * tilemap->tileset->scale};

    for (Uint32 row = 0; row < tilemap->height; ++row) {
	for (Uint32 column = 0; column < tilemap->width; ++column) {
	    dest_region.x = x + column * tilemap->tileset->tile_size * tilemap->tileset->scale;
	    dest_region.y = y + row * tilemap->tileset->tile_size * tilemap->tileset->scale;
	    /* Get the indices of the tiles to use. */
	    tile_region.x = tilemap->layout[(row*tilemap->width) + column].x * tilemap->tileset->tile_size;
	    tile_region.y = tilemap->layout[(row*tilemap->width) + column].y * tilemap->tileset->tile_size;
	    /* Render. */
	    SDL_RenderCopy(ren, tilemap->tileset->tiles, &tile_region, &dest_region);
	}
    }
}
